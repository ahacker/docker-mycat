# docker-mycat

### MYSQL 部分配置：

#### 主库：
```
# 创建从库连接的用户
GRANT REPLICATION SLAVE ON *.* TO 'backup'@'%' IDENTIFIED BY '123456';

# 刷新权限表
FLUSH PRIVILEGES;

# 查看主库同步信息
SHOW MASTER STATUS;

File              Position  Binlog_Do_DB  Binlog_Ignore_DB  Executed_Gtid_Set  
----------------  --------  ------------  ----------------  -------------------
mysql-bin.000006       592                                                     
```

#### 从库：

```
# 关闭slave
STOP SLAVE;

# 配置主库的连接信息
# MASTER_HOST：填写的是docker服务的名称
# MASTER_PORT：端口号要对应主库开放的端口号
# MASTER_LOG_FILE：对应到主库 SHOW MASTER STATUS 查询出来的日志文件
# MASTER_LOG_POS：对应到主库 SHOW MASTER STATUS 查询出来的位置
CHANGE MASTER TO MASTER_HOST='mysql-master',MASTER_PORT=4001,MASTER_USER='backup',MASTER_PASSWORD='123456',MASTER_LOG_FILE='mysql-bin.000006',MASTER_LOG_POS=592;

# 开启slave
START SLAVE;

# 查看从库同步信息
SHOW SLAVE STATUS;


#出现问题时，执行：
#错误代码： 1872
#Slave failed to initialize relay log info structure from the repository

RESET SLAVE;

```

#### 构建mycat镜像：
```
docker-compose build mycat
```

#### 启动服务：
```
docker-compose up -d
```
